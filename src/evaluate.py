import itertools

import matplotlib.pyplot as plt
import numpy as np
from dvclive import Live
from sklearn.metrics import confusion_matrix
from tensorflow import keras

with open("data/t10k-images-idx3-ubyte", "rb") as f:
    x_test = np.array(np.frombuffer(f.read(), np.uint8, offset=16))
    x_test = np.reshape(x_test, (-1, 28, 28, 1)).astype(np.float32) / 255

with open("data/t10k-labels-idx1-ubyte", "rb") as f:
    y_test = np.array(np.frombuffer(f.read(), np.uint8, offset=8))
    y_test = keras.utils.to_categorical(y_test, num_classes=10)

model = keras.models.load_model("models/model.h5")

with Live("eval") as live:
    loss, accuracy = model.evaluate(x_test, y_test)
    live.log_metric("loss", loss)
    live.log_metric("accuracy", accuracy)

Y_pred = model.predict(x_test)
Y_pred_classes = np.argmax(Y_pred, axis=1)
Y_true = np.argmax(y_test, axis=1)

cm = confusion_matrix(Y_true, Y_pred_classes)

plt.imshow(cm, interpolation="nearest", cmap="Blues")
plt.title("Confusion matrix")
tick_marks = np.arange(10)
plt.xticks(tick_marks, range(10), rotation=45)
plt.yticks(tick_marks, range(10))

thresh = cm.max() / 2.0
for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
    plt.text(
        j,
        i,
        cm[i, j],
        horizontalalignment="center",
        color="white" if cm[i, j] > thresh else "black",
    )

plt.ylabel("True label")
plt.xlabel("Predicted label")

plt.savefig("eval/matrix.png")

errors = Y_pred_classes - Y_true != 0

Y_pred_classes_errors = Y_pred_classes[errors]
Y_pred_errors = Y_pred[errors]
Y_true_errors = Y_true[errors]
X_test_errors = x_test[errors]

# Probabilities of the wrong predicted numbers
Y_pred_errors_prob = np.max(Y_pred_errors, axis=1)

# Predicted probabilities of the true values in the error set
true_prob_errors = np.diagonal(np.take(Y_pred_errors, Y_true_errors, axis=1))

# Difference between the probability of the predicted label and the true label
delta_pred_true_errors = Y_pred_errors_prob - true_prob_errors

# Sorted list of the delta prob errors
sorted_dela_errors = np.argsort(delta_pred_true_errors)

# Top 5 errors
most_important_errors = sorted_dela_errors[-5:]

fig, ax = plt.subplots(1, 5, sharex=True, sharey=True)

for col in range(5):
    error = most_important_errors[col]
    ax[col].imshow((X_test_errors[error]).reshape((28, 28)), cmap="Greys")
    ax[col].set_title(
        f"Pred:{Y_pred_classes_errors[error]}\nTrue:{Y_true_errors[error]}"
    )

fig.tight_layout()
fig.savefig("eval/top-errors.png")
