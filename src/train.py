import numpy as np
import yaml
from tensorflow import keras

params = yaml.safe_load(open("params.yaml", encoding="utf-8"))
print("Parameters: ", params)

with open("data/train-images-idx3-ubyte", "rb") as f:
    x_train = np.array(np.frombuffer(f.read(), np.uint8, offset=16))
    x_train = np.reshape(x_train, (-1, 28, 28, 1)).astype(np.float32) / 255

with open("data/train-labels-idx1-ubyte", "rb") as f:
    y_train = np.array(np.frombuffer(f.read(), np.uint8, offset=8))
    y_train = keras.utils.to_categorical(y_train, num_classes=10)

model = keras.Sequential(
    [
        keras.Input(shape=(28, 28, 1)),
        keras.layers.Conv2D(params["depth"], kernel_size=(3, 3), activation="relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2)),
        keras.layers.Conv2D(params["depth"] * 2, kernel_size=(3, 3), activation="relu"),
        keras.layers.MaxPooling2D(pool_size=(2, 2)),
        keras.layers.Flatten(),
        keras.layers.Dropout(params["dropout"]),
        keras.layers.Dense(units=10, activation="softmax"),
    ]
)

model.summary()

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

model.fit(
    x_train,
    y_train,
    batch_size=128,
    epochs=params["epochs"],
    validation_split=0.1,
    verbose=1,
)

model.save("models/model.h5")
